#Project Frozen Fist

##VortexOps

###Pushed to BitBucket Repository

### Modified README on BitBucket 2/25/2018

Frozen Fist is a project for designing the software that controls armored assault vehicles from remote locations.

Contact me via [my email](brady.hudson@smail.rasmussen.edu) for more information.

![An armored assault vehicle](https://www.armormax.com/file/2017/08/xbatmobile-09-600x340.jpg.pagespeed.ic.-6ZTCm08ZO.webp)
